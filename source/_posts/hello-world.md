---
title: RecoilCTF... Hello World!
date: 2019-06-03 12:58:16
tags:
---


## About this Challenge

RecoilCTF is divided into several categories: Crypto, Misc, Pwn, PPC, and RE. This post will focus on the challenge "Hello World!" found under the PPC category. 

## Hello, World!

As we open the challenge we're given a simple .cpp file called "hello_world.cpp". Seems easy enough, let's download it by clicking on the button in the middle. After it's download, we'll just open it (I really like to use Sublime Text as an IDE, but Visual Studio or even Notepad will be fine). 

But wait... this is just a simple C++ program that prints "Hello, World!" into the console... dang, I guess we'll just give up and try the "Welcome" challenge instead. :/

## WRONG! 

Let's keep going. 

There's obviously something more to it than that, let's get creative. 

After you explore the file a little bit, you notice there is a ton of spaces and tabs in the file. 

![](/images/hello_world.jpg)

Wow, that's odd, I wonder what that could mean. It must mean something. I'm going to google "whitespace programming" and see what comes up.


## Whitespace: An actual programming language. 

Yep. You guessed it. There's an actual esoteric programming language called Whitespace. It takes a sequence of spaces and tabs and organizes the memory stack through those spaces an tabs. For more info, I'd recommend the following links:

[- Whitespace according to Wikepedia](https://en.wikipedia.org/wiki/Whitespace_(programming_language))
[- If you actually wanted to learn some Whitespace](https://h0tsh0tt.wordpress.com/2016/07/03/whitespace-language-tutorial/)
[- A short video on Whitespace](https://www.youtube.com/watch?v=ix8vyvACVQ0)

Thankfully, there are whitespace compilers online. This is the one I used.

[Whitespace Online Compiler](https://vii5ard.github.io/whitespace/)


Once you get to this site, you can click outside the intro block and you can get straight to compiling. Go to your hello_world.cpp file, press Ctrl+A to highlight everything (spaces, tabs, and all), copy it and paste it into the compiler. 


Let's run it by clicking the "Run" button near the upper left. In the output box below, we come up with the following printed below:

``` bash
Nothing to see here. Maybe youre not looking in the right place?
```

Hmm... so where else can we look?

Thankfully, the Whitespace IDE I used has a Memory button that displays what is on the stack to the right of the Output button. Let's click it.


## The Memory Stack

Once we click the button, you should see something like this:

``` bash
Stack: [125, 101, 67, 52, 112, 115, 51, 116, 49, 104, 119, 95, 99, 49, 114, 51, 84, 48, 115, 101, 95, 48, 115, 123, 102, 116, 99, 108, 105, 111, 99, 101, 114]
```

What could these numbers mean? It turns out that these numbers correspond with the ASCII table. 

Here's a neat website that I found that converts these ASCII decimals to a more readable format. 

[ASCII Converter](https://www.branah.com/ascii-converter)

Copy and paste the stack array into the Decimal text box in the middle of the page. The converter will automatically convert the numbers pasted to its actual meaning in the top block, which should read like this:

``` bash
[}, e, C, 4, p, s, 3, t, 1, h, w, _, c, 1, r, 3, T, 0, s, e, _, 0, s, {, f, t, c, l, i, o, c, e, r]
```

Since the memory stack is generally posed in little-endian format, the ASCII variables will be put in backwards in order to be eventually printed normally. So due to the EOF error that occurred while compiling, the stack was never printed (lucky us!). 

Once you read the output backwards in the top block of the converter, the flag will be right there!

``` bash
recoilctf{es0T3r1c_wh1t3sp4Ce}
```

And there you go! Hope this was a good guide to solving this challenge. 


